package me.flyray.bsin.facade.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

/**
 * @author huangzh
 * @ClassName ModelTypeSVC
 * @DATE 2020/9/28 15:15
 */

@Path("modelType")
public interface BsinAdminModelTypeService {
    /**
     * 查看所有模型类型
     * @param requestMap
     * @return
     */
    @POST
    @Path("getModelTypeTree")
    @Produces("application/json")
    Map<String, Object> getModelTypeTree(Map<String, Object> requestMap);

    /**
     * ID,名称模糊，编号查看模型类型
     * @param requestMap
     * @return
     */
    @POST
    @Path("getModelModelTypeList")
    @Produces("application/json")
    Map<String, Object> getModelTypeList(Map<String, Object> requestMap);

    /**
     * 生成新的模型类型
     * @param requestMap
     * @return
     */
    @POST
    @Path("add")
    @Produces("application/json")
    Map<String, Object> add(Map<String, Object> requestMap);

    /**
     * 修改模型类型
     * @param requestMap
     * @return
     */
    @POST
    @Path("edit")
    @Produces("application/json")
    Map<String, Object> edit(Map<String, Object> requestMap);

    /**
     * 删除模型类型
     * @param requestMap
     * @return
     */
    @POST
    @Path("delete")
    @Produces("application/json")
    Map<String, Object> delete(Map<String, Object> requestMap);
}
