package me.flyray.bsin.gateway.service.impl;

import com.alipay.sofa.rpc.api.GenericService;
import com.alipay.sofa.rpc.config.ConsumerConfig;
import com.alipay.sofa.rpc.registry.RegistryFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.seata.saga.engine.StateMachineEngine;
import io.seata.saga.statelang.domain.ExecutionStatus;
import io.seata.saga.statelang.domain.StateMachineInstance;
import me.flyray.bsin.gateway.common.ApiResult;
import me.flyray.bsin.gateway.domain.BsinChoreographyServiceDo;
import me.flyray.bsin.gateway.pojo.Parameter;
import me.flyray.bsin.gateway.service.BsinStateMachineServiceTaskProxy;
import me.flyray.bsin.utils.BsinServiceInvokeUtil;

/**
 * 编排界面配置的任务节点服务，通过固定的代理服务调用RPC服务
 */
//@Service
public class BsinStateMachineServiceTaskProxyImpl implements BsinStateMachineServiceTaskProxy {

    public static ApplicationContext applicationContext;
    public static StateMachineEngine stateMachineEngine;
    @Autowired
    public BsinServiceInvokeUtil bsinServiceInvoke;

    private static ConcurrentHashMap<String, GenericService> concurrentHashMapBolt = new ConcurrentHashMap<>();

    //map传参
    @Override
    public Object execute(Map<String, Object> parameter) {
        String serviceName = (String) parameter.get("serviceName");
        String serviceMethod = (String) parameter.get("serviceMethod");
        String version = (String) parameter.get("version");
        Map<String, Object> param = (Map<String, Object>) parameter.get("param");

        Object result = bsinServiceInvoke.genericInvoke(serviceName,serviceMethod,"1.0", param);
        System.out.println(result);
        return result;
    }


    //对象入参
    @Override
    public Object execute(Parameter parameter) {

        Object[] params = new Object[parameter.getParam().size()];
        Map<String, Object> reqParams = new HashMap<>();
        int i = 0;
        for (String paramName : parameter.getParam().keySet()
        ) {
            params[i] = parameter.getParam().get(paramName);
            i++;
            reqParams.put(paramName,params[i]);
        }

        String serviceName = parameter.getServiceName();
        String serviceMethod = parameter.getServiceMethod();
        String version = parameter.getVersion();
        Object result = bsinServiceInvoke.genericInvoke(serviceName, serviceMethod, version, reqParams);
        System.out.println(result);
        return result;
    }


    /**
     * 编排服务调用
     */
    @Override
    public ApiResult choreographyInvoke(BsinChoreographyServiceDo choreographyServiceDo, Map<String, Object> reqParam) {
        String stateMachineName = choreographyServiceDo.getStateMachineName();
        String businessKey = String.valueOf(System.currentTimeMillis());
        reqParam.put("businessKey", businessKey);
        //sync  同步
        StateMachineInstance inst = stateMachineEngine.startWithBusinessKey(stateMachineName, null,
                businessKey, reqParam);
        Assert.isTrue(ExecutionStatus.SU.equals(inst.getStatus()),
                "saga transaction execute failed. XID: " + inst.getId());
        System.out.println("saga transaction commit succeed. XID: " + inst.getId());
        return ApiResult.ok("saga transaction commit succeed. XID: " + inst.getId());
    }

}


