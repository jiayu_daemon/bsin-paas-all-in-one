package me.flyray.bsin.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.MultipartAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import io.seata.saga.engine.StateMachineEngine;
import me.flyray.bsin.gateway.config.AliOSSProperties;
import me.flyray.bsin.gateway.config.MessageProperties;
import me.flyray.bsin.gateway.portal.BsinAdminPortal;
import me.flyray.bsin.gateway.service.impl.BsinStateMachineServiceTaskProxyImpl;


@ImportResource({"classpath*:gateway-starter.xml"})
@EnableConfigurationProperties({MessageProperties.class, AliOSSProperties.class})
@SpringBootApplication(exclude = {MultipartAutoConfiguration.class})
@ComponentScan("me.flyray.bsin.*")
public class BsinGatewayApplication {

    public static void main(String[] args) {

        SpringApplication.run(BsinGatewayApplication.class, args);
        // BsinStateMachineServiceTaskProxyImpl.applicationContext = SpringApplication.run(BsinGatewayApplication.class, args);
        // 解决找不到stateMachineEngine问题
//        BsinStateMachineServiceTaskProxyImpl.stateMachineEngine = (StateMachineEngine) BsinStateMachineServiceTaskProxyImpl.applicationContext.getBean("stateMachineEngine");

    }

    /**
     * 显示声明CommonsMultipartResolver为mutipartResolver
     * 配置上传文件大小
     */
    @Bean(name = "multipartResolver")
    public MultipartResolver multipartResolver() {
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        //resolver.setDefaultEncoding("UTF-8");
        //resolveLazily属性启用是为了推迟文件解析，以在在UploadAction中捕获文件大小异常
        resolver.setResolveLazily(true);
        resolver.setMaxInMemorySize(40960);
        resolver.setMaxUploadSize(100 * 1024 * 1024);//上传文件大小 20M 20*1024*1024
        return resolver;
    }

}

