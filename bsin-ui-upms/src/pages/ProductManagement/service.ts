import request from '../../utils/bsinRequest';

// 获取字典列表
export const getPageList = (params: any) => {
  return request('/getPageList', {
    serviceName: 'ProductService',
    methodName: 'getPageList',
    bizParams: {
      ...params,
    },
  });
};

// 新增字典
export const addPageList = (params: any) => {
  return request('/addPageList', {
    serviceName: 'ProductService',
    methodName: 'add',
    bizParams: {
      ...params,
    },
  });
};

// 编辑字典
export const editPageList = (params: any) => {
  return request('/editPageList', {
    serviceName: 'ProductService',
    methodName: 'edit',
    bizParams: {
      ...params,
    },
  });
};

// 删除字典
export const deletePageList = (params: any) => {
  return request('/editPageList', {
    serviceName: 'ProductService',
    methodName: 'delete',
    bizParams: {
      ...params,
    },
  });
};

// 字典项查询
export const getDictItemPageList = (params: any) => {
  return request('/getDictItemPageList', {
    serviceName: 'ProductService',
    methodName: 'getProductAppPageList',
    bizParams: {
      ...params,
    },
  });
};

// 字典项新增
export const addDictItemPageList = (params: any) => {
  return request('/addItem', {
    serviceName: 'ProductService',
    methodName: 'addProductApp',
    bizParams: {
      ...params,
    },
  });
};

// 字典项编辑
export const editDictItemPageList = (params: any) => {
  return request('/editItem', {
    serviceName: 'ProductService',
    methodName: 'editProductApp',
    bizParams: {
      ...params,
    },
  });
};

// 字典项删除
export const deleteDictItemPageList = (params: any) => {
  return request('/deleteItem', {
    serviceName: 'ProductService',
    methodName: 'deleteProductApp',
    bizParams: {
      ...params,
    },
  });
};


export const getAppList = (params) => {
  return request('/list', {
    serviceName: 'AppService',
    methodName: 'getPublishedApps',
    bizParams: {
      ...params,
    },
  });
};