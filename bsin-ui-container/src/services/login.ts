import bsinRequest from '@/utils/bsinRequest';

// 登录接口
export const userLogin = (params: any) => {
  return bsinRequest('/login', {
    serviceName: 'UserService',
    methodName: 'login',
    bizParams: {
      ...params,
    },
  });
};

// 获取所有租户
export const getTenantList = (params: any) => {
  return bsinRequest('/getAllTenantList', {
    serviceName: 'TenantService',
    methodName: 'getAllTenantList',
    bizParams: {
      ...params,
    },
  });
};

export const getTenantBaseApp = (params: any) => {
  return bsinRequest('/getTenantBaseApp', {
    serviceName: 'TenantService',
    methodName: 'getTenantBaseApp',
    bizParams: {
      ...params,
    },
  });
};

// 商户登录
export const merchantLogin = (params: any) => {
  return bsinRequest('/login', {
    serviceName: 'MerchantService',
    methodName: 'login',
    version: '1.0',
    bizParams: {
      ...params,
    },
  });
};
