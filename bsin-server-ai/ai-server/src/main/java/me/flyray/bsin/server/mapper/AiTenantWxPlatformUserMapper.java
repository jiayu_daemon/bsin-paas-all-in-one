package me.flyray.bsin.server.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

import me.flyray.bsin.server.domain.TenantWxPlatformUser;

/**
* @author bolei
* @description 针对表【ai_tenant_wxmp_user】的数据库操作Mapper
* @createDate 2023-04-28 14:02:38
* @Entity generator.domain.AiTenantWxmpUser
*/

@Mapper
@Repository
public interface AiTenantWxPlatformUserMapper {

    void insert(TenantWxPlatformUser tenantWxPlatformUser);

    void deleteById(String serialNo);

    TenantWxPlatformUser selectByOpenId(String openId);

    List<TenantWxPlatformUser> selectPageList(@Param("tenantId") String tenantId, @Param("name") String name, @Param("phone") String code);

}
